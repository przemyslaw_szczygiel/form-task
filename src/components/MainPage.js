import React, { useState } from 'react';
import styles from '../components/MainPage.module.css';
import Header from './Header';
import Form from './Form';
import SuccessMessage from './SuccessMessage';
import ErrorMessage from './ErrorMessage';

const MainPage = () => {
    const [isFormVisible, setIsFormVisible] = useState(true);
    const [isSuccessMessageVisible, setIsSuccessMessageVisible] = useState(false);
    const [isErrorMessageVisible, setIsErrorMessageVisible] = useState(false);

    async function addDataHandler(data) {
        try {
            const response = await fetch('https://form-data-f2c25-default-rtdb.firebaseio.com/form-data.json',
                {
                    method: 'POST',
                    body: JSON.stringify(data),
                    headers: {
                        'Content-Type': 'application/json'
                    }
                });

            const dataToSend = await response.json();
            
            if(response.ok) {
                setIsSuccessMessageVisible(true);
                setIsFormVisible(false);
            } else {
                throw new Error('Something went wrong!');
            }
            console.log(dataToSend);
        } catch(error) {
            console.log(error);
            setIsErrorMessageVisible(true);
        }    
    } 

    return (
        <div className={styles['main-page']}>
            <Header />
            {isFormVisible ? <Form onAddData={addDataHandler}/> : null}
            {isSuccessMessageVisible ? <SuccessMessage /> : null}
            {isErrorMessageVisible ? <ErrorMessage /> : null}
        </div>
    );
}

export default MainPage;