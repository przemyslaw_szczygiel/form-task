import React, { useState } from 'react';
import styles from '../components/Form.module.css';

const Form = (props) => {
    const [enteredDescription, setEnteredDescription] = useState('');
    const [enteredConfirmation, setEnteredConfirmation] = useState('');
    const [chosenVat, setChosenVat] = useState('');
    const [enteredNetto, setEnteredNetto] = useState('');
    const [brutto, setBrutto] = useState('');
    const [charCount, setCharCount] = useState(255);
    
    const descriptionChangeHandler = (event) => {
        setEnteredDescription(event.target.value)
    }

    const confirmationChangeHandler = (event) => {
        setEnteredConfirmation(event.target.value);
    }

    const vatChangeHandler = (event) => {
        setChosenVat(event.target.value);
        if(chosenVat != null) {
            document.getElementById('netto_id').disabled = false; 
        }
    }

    const nettoChangeHandler = (event) => {
        let nettoValue = event.target.value;
     
        setEnteredNetto(nettoValue);
        let vat = document.getElementById('vat').value;
        // calculate brutto from vat and netto entered by user
        let brutto = Number(nettoValue) + (Number(nettoValue) * vat)
        setBrutto(brutto);
        // console.log(brutto);
        // console.log(typeof brutto);
    }

    const submitHandler = (event) => {
        event.preventDefault();

        const enteredData = {
            description: enteredDescription,
            confirmation: enteredConfirmation,
            vat: chosenVat,
            netto: enteredNetto,
            brutto: brutto
        }

        let formIsValid;
        if(enteredDescription == null) {
            alert('Text is required!');
        }
        if(setEnteredConfirmation == null) {
            alert('Text is required!');
        } 
        if(chosenVat == null) {
            alert('Text is required!');
        }
        if(enteredNetto == null) {
            alert('Text is required!');
        } else {
            formIsValid = true;
                if(formIsValid) {
                console.log(enteredData);
                props.onAddData(enteredData);
            }     
        }
    }

    const textAreaCheck = (element) => {
        let text_area = element.target.value.length;
        setCharCount(254 - text_area);
        
        if(text_area >= 255) { 
            alert('You can\'t enter more than 255 characters');
        }
    }

    return (
        <form onSubmit={submitHandler} className={styles['form']}>
            <label>Description:</label>
            <textarea
                maxLength='255'
                onChange={descriptionChangeHandler}
                onKeyPress={textAreaCheck}
                required
            />
            <p><b>{charCount}</b> characters left...</p>
            
            <div className={styles['custom-item']} onChange={confirmationChangeHandler}>
            <label>Send confirmation:</label>
                <input
                    type='radio'
                    name='confirmation'
                    value='YES'
                    autoComplete='off'
                    required
                />YES
            
                <input
                    type='radio'
                    value='No'
                    name='confirmation'
                    autoComplete='off'
                    required
                />NO
            </div>
      
            <div className={styles['custom-item']}>
                <label htmlFor='vat'>VAT:</label>
                <select name='vat' id='vat' onChange={vatChangeHandler} required>
                    <option value='' selected disabled hidden>Choose VAT</option>
                    <option value='0.19'>19%</option>
                    <option value='0.21'>21%</option>
                    <option value='0.23'>23%</option>
                    <option value='0.25'>25%</option>
                </select>
            </div>

            <label>Price netto EUR:</label>
            <input
                id='netto_id'
                type='number'
                pattern='[0-9]+'
                onChange={nettoChangeHandler}
                disabled
                required
            />

            <label>Price brutto EUR:</label>
            <input
                id='brutto_id' 
                type='number'
                value={brutto}
                disabled
            />

            <button type='submit'>Submit</button>
        </form>
    )
}

export default Form;

