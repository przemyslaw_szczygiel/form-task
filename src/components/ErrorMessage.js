import React from 'react';
import styles from '../components/ErrorMessage.module.css';

const ErrorMessage = () => {
    
    return (
        <div  className={styles['error-message']}>
            <h2>Ooops!</h2>
            <p>Unfortunately are not able to submit form. Try again!</p>
        </div>
    )
}

export default ErrorMessage;