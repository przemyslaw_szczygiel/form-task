import React from 'react';
import styles from '../components/SuccessMessage.module.css';

const SuccessMessage = () => {
    return (
        <div className={styles['success-message']}>
            <h2>Congratulations!</h2>
            <p>You've successfully submitted your form.</p>
        </div>
    )
}

export default SuccessMessage;