**Form Task - small project in React.js**

1. clone the repository into Your local disk

2. install/update react.js on your computer (about React: https://reactjs.org/docs/getting-started.html) 

3. run the application typing 'npm start' in project directory in terminal

4. open [http://localhost:3000](http://localhost:3000) to view it in your browser.

5. You should fill in the whole form and click 'Submit' button

6. The data is sent into Firebase (the link is located in addDataHandler function in MainPage.js file)

7. After the data is submitted succesfully You should see a green box with congratulations :)
